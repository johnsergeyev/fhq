package sergeyev.john.nfcscanner;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import sergeyev.john.nfcscanner.classes.AppClass;
import sergeyev.john.nfcscanner.classes.AppConstants;
import sergeyev.john.nfcscanner.classes.ResponseObject;
import sergeyev.john.nfcscanner.classes.ServerService;
import sergeyev.john.nfcscanner.classes.ServiceResultReciever;
import sergeyev.john.nfcscanner.classes.Utils;

public class DataActivity extends AppCompatActivity implements AppConstants, ServiceResultReciever.Reciever {
    private int mTagType;
    private ServiceResultReciever mReciever;
    private EditText mETSurname;
    private EditText mETName;
    private EditText mETSecondName;
    private EditText mETStages;
    private EditText mETPlace;
    private TextView mSubscriptionStatus;
    private ProgressBar mProgressBar;
    private LinearLayout mStagesLayout;
    private LinearLayout mPlaceLayout;
    private LinearLayout mSubscriptionLayout;
    private Button mBtn;
    private String mTagId;

    private String mStage;
    private int mPlace;
    private int mSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        mReciever = new ServiceResultReciever(new Handler());
        mReciever.setReceiver(this);

        mSubscriptionStatus = (TextView) findViewById(R.id.subscription_status);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mETSurname = (EditText) findViewById(R.id.surname);
        mETName = (EditText) findViewById(R.id.name);
        mETSecondName = (EditText) findViewById(R.id.second_name);
        mETSecondName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptAuth();
                    return true;
                }
                return false;
            }
        });
        mETStages = (EditText) findViewById(R.id.stages);
        mETPlace = (EditText) findViewById(R.id.place);
        mPlaceLayout = (LinearLayout) findViewById(R.id.place_layout);
        mStagesLayout = (LinearLayout) findViewById(R.id.stages_layout);
        mSubscriptionLayout = (LinearLayout) findViewById(R.id.subscription_layout);
        mBtn = (Button) findViewById(R.id.btn);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTagType != TAG_KNOWN_REGISTERED) {
                    attemptAuth();
                } else {
                    attemptSubscribe();
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mTagType = extras.getInt(TAG_TYPE);
            mTagId = extras.getString(TAG);
            mPlace = extras.getInt(PLACE);
            mStage = extras.getString(STAGE);
            mSubscription = extras.getInt(SUBSCRIPTION);
            mETStages.setText(String.valueOf(mStage));
            mETPlace.setText(String.valueOf(mPlace));
            mETName.setText(extras.getString(NAME));
            mETSurname.setText(extras.getString(SURNAME));
            mETSecondName.setText(extras.getString(SECONDNAME));
        } else {
            mTagType = TAG_UNKNOWN;
            mTagId = "";
        }

        setupActionBar();
        setProgress(false);
    }

    private void setProgress(final boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);

        if (mTagType == TAG_UNKNOWN) {
            mPlaceLayout.setVisibility(View.GONE);
            mStagesLayout.setVisibility(View.GONE);
            mSubscriptionLayout.setVisibility(View.GONE);

            if (show) {
                Utils.disableEditText(mETSurname, true);
                Utils.disableEditText(mETName, true);
                Utils.disableEditText(mETSecondName, true);
            } else {
                Utils.enableEditText(mETSurname);
                Utils.enableEditText(mETName);
                Utils.enableEditText(mETSecondName);
            }
        } else {
            if (mTagType == TAG_KNOWN_REGISTERED) {
                mBtn.setText(R.string.give_subscription);
                if (mSubscription == 1) {
                    mBtn.setVisibility(View.GONE);
                    mSubscriptionStatus.setText(R.string.subscription_ok);
                } else if (mSubscription == 2) {
                    mSubscriptionStatus.setText(R.string.subscription_err);
                    if (!show) {
                        mBtn.setVisibility(View.VISIBLE);
                    } else {
                        mBtn.setVisibility(View.GONE);
                    }
                } else {
                    mSubscriptionLayout.setVisibility(View.GONE);
                    mBtn.setVisibility(View.GONE);
                }
            } else {
                mBtn.setText(R.string.auth_user);
                mSubscriptionLayout.setVisibility(View.GONE);
            }

            if (mTagType == TAG_KNOWN_REGISTERED) {
                if (mPlace == 0) {
                    mPlaceLayout.setVisibility(View.GONE);
                    mBtn.setVisibility(View.GONE);
                    mSubscriptionLayout.setVisibility(View.GONE);
                } else {
                    mStagesLayout.setVisibility(View.GONE);
                }
            } else {
                mPlaceLayout.setVisibility(View.GONE);
                mStagesLayout.setVisibility(View.GONE);
            }

            Utils.disableEditText(mETSurname, true);
            Utils.disableEditText(mETName, true);
            Utils.disableEditText(mETSecondName, true);
            Utils.disableEditText(mETStages, true);
            Utils.disableEditText(mETPlace, true);
        }
    }

    private void attemptSubscribe() {
        if (!app().isOnline()) {
            noConnection();
            return;
        }

        setProgress(true);
        subscribeUser();
    }

    private void attemptAuth() {
        if (!app().isOnline()) {
            noConnection();
            return;
        }

        mETSurname.setError(null);
        mETName.setError(null);
        mETSecondName.setError(null);

        String f = mETSurname.getText().toString();
        String i = mETName.getText().toString();
        String o = mETSecondName.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(f)) {
            mETSurname.setError(getString(R.string.error_field_required));
            focusView = mETSurname;
            cancel = true;
        }

        if (TextUtils.isEmpty(i)) {
            mETName.setError(getString(R.string.error_field_required));
            focusView = mETName;
            cancel = true;
        }

        if (TextUtils.isEmpty(o)) {
            mETSecondName.setError(getString(R.string.error_field_required));
            focusView = mETSecondName;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            setProgress(true);
            authUser(f, i, o);
        }
    }

    private void authUser(String f, String i, String o) {
        Intent intent = new Intent(DataActivity.this, ServerService.class);
        intent.putExtra(RECIEVER, mReciever);
        intent.putExtra(METHOD, AUTH_TAG_METHOD);
        intent.putExtra(PARAMS, formAuthString(f, i, o));
        startService(intent);
    }

    private String formAuthString(String f, String i, String o) {
        if (mTagType == TAG_UNKNOWN) {
            return Utils.concatStrings("markId=", mTagId, "&f=", f, "&i=", i, "&o=", o);
        } else {
            return Utils.concatStrings("markId=", mTagId);
        }
    }

    private void subscribeUser() {
        Intent intent = new Intent(DataActivity.this, ServerService.class);
        intent.putExtra(RECIEVER, mReciever);
        intent.putExtra(METHOD, ADMIN_SUB_METHOD);
        intent.putExtra(PARAMS, formSubscribeString());
        startService(intent);
    }

    private String formSubscribeString() {
        return Utils.concatStrings("markId=", mTagId);
    }

    private AppClass app() {
        return (AppClass) getApplication();
    }

    private void noConnection() {
        Toast.makeText(getBaseContext(), R.string.error_no_connection, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle(R.string.tag_data);
    }

    private void tokenError() {
        Toast.makeText(getBaseContext(), R.string.token_error, Toast.LENGTH_SHORT).show();
        setResult(TOKEN_ERROR);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRecieveResult(int resultCode, Bundle resultData) {
        ResponseObject response = new ResponseObject(resultData.getString(RESPONSE));

        if (resultCode == STATUS_NO_CONNECTION) {
            setProgress(false);
            noConnection();
            return;
        } else try {
            if (resultCode == STATUS_ERROR && response.json().getInt(ERROR_CODE) == 100) {
                tokenError();
                return;
            }
        } catch (JSONException e) {

        }

        if (response.method().equals(AUTH_TAG_METHOD)) {
            if (resultCode == STATUS_OK) {
                Toast.makeText(getBaseContext(), R.string.registered, Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            } else {
                setProgress(false);
                mETSurname.setError(null);
                mETName.setError(null);
                mETSecondName.setError((CharSequence) response.getParam(ERROR_STRING, STRING));
            }
        } else if (response.method().equals(ADMIN_SUB_METHOD)) {
            if (resultCode == STATUS_OK) {
                Toast.makeText(getBaseContext(), R.string.subscription_ok, Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            } else {
                Toast.makeText(getBaseContext(), (CharSequence) response.getParam(ERROR_STRING, STRING), Toast.LENGTH_SHORT).show();
                setProgress(false);
            }
        }
    }
}
