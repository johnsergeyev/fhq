package sergeyev.john.nfcscanner;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import sergeyev.john.nfcscanner.classes.AppClass;
import sergeyev.john.nfcscanner.classes.AppConstants;
import sergeyev.john.nfcscanner.classes.ResponseObject;
import sergeyev.john.nfcscanner.classes.ServerService;
import sergeyev.john.nfcscanner.classes.ServiceResultReciever;
import sergeyev.john.nfcscanner.classes.Utils;

public class LoginActivity extends AppCompatActivity implements AppConstants, ServiceResultReciever.Reciever {

    private EditText mLoginView;
    private EditText mPasswordView;
    private int mLoginType;
    private ServiceResultReciever mReciever;
    private Handler _handler;
    private Boolean _lockLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupActionBar();

        _lockLocation = false;

        _handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Bundle b = msg.getData();

                switch (msg.what) {
                    case LOCATION_ERROR:
                        locationError(b.getDouble(DISTANCE));
                        finish();
                        return true;
                    case LOCATION_OK:
                        startReader();
                        return true;
                    case LOCATION_NO_SIGNAL:
                        signalError();
                        finish();
                        return true;
                }
                return false;
            }
        });

        mReciever = new ServiceResultReciever(new Handler());
        mReciever.setReceiver(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mLoginType = extras.getInt(LOGIN_TYPE);
        } else {
            mLoginType = LOGIN_COACH;
        }

        // Set up the login form.
        mLoginView = (EditText) findViewById(R.id.login);
        mLoginView.setHint(mLoginType == LOGIN_ADMIN ? R.string.prompt_login_admin : R.string.prompt_login_coach);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mLoginSignInButton = (Button) findViewById(R.id.login_sign_in_button);
        mLoginSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        setProgress(false);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLoginType == LOGIN_COACH && !_lockLocation) {
            app().stopLocation();
        }
    }

    private AppClass app() {
        return (AppClass) getApplication();
    }

    private void loginUser(String login, String password) {
        Intent intent = new Intent(LoginActivity.this, ServerService.class);
        intent.putExtra(RECIEVER, mReciever);
        intent.putExtra(METHOD, LOGIN_METHOD);
        intent.putExtra(PARAMS, formLoginString(login, password, mLoginType));
        startService(intent);
    }

    private String formLoginString(String l, String p, int t) {
        return Utils.concatStrings("user=", l, "&pass=", p, "&role=", t == LOGIN_ADMIN ?
                LOGIN_ADMIN_TEXT : LOGIN_COACH_TEXT);
    }

    private String formAuthString(String c) {
        return Utils.concatStrings("caption="+c);
    }

    private void startReader() {
        Intent intent = new Intent(getBaseContext(), NFCReadActivity.class);
        intent.putExtra(LOGIN_TYPE, mLoginType);
        startActivity(intent);
        _lockLocation = true;
        finish();
    }

    private void noConnection() {
        Toast.makeText(getBaseContext(), R.string.error_no_connection, Toast.LENGTH_SHORT).show();
    }

    private void signalError() {
        Toast.makeText(getBaseContext(), R.string.error_location_settings, Toast.LENGTH_SHORT).show();
    }

    private void locationError(double d) {
        Toast.makeText(getBaseContext(), Utils.concatStrings(getString(R.string.error_location), " ", String.valueOf(Math.round(d)), "м"), Toast.LENGTH_SHORT).show();
    }

    private void authDevice(ResponseObject response) {
        app().setToken((String) response.getParam(TOKEN, STRING));
        String id = app().getDeviceId();
        String caption = app().getCaption();

        if (mLoginType == LOGIN_COACH) {
            try {
                JSONObject task = (JSONObject) response.getParam(TASK, OBJECT);
                JSONObject coords = (JSONObject) task.getJSONObject(COORDS);
                String user_id = (String) response.getParam(USER_ID, STRING);

                app().setCoachParams(coords.getDouble(LAT), coords.getDouble(LONG), task.getDouble(DISTANCE), user_id);
            } catch (JSONException e) {

            }
        }

        Intent intent = new Intent(LoginActivity.this, ServerService.class);
        intent.putExtra(RECIEVER, mReciever);
        intent.putExtra(METHOD, DEVICE_AUTH_METHOD);
        intent.putExtra(PARAMS, formAuthString(caption));
        startService(intent);
    }

    private void attemptLogin() {
        if (!app().isOnline()) {
            noConnection();
            return;
        }

        mLoginView.setError(null);
        mPasswordView.setError(null);

        String login = mLoginView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(login)) {
            mLoginView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            setProgress(true);
            loginUser(login, password);
        }
    }

    private void setProgress(final boolean show) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.login_progress);
        Button btn = (Button) findViewById(R.id.login_sign_in_button);

        pb.setVisibility(show?View.VISIBLE:View.GONE);
        btn.setVisibility(show ? View.GONE : View.VISIBLE);
        if (show) {
            Utils.disableEditText(mLoginView);
            Utils.disableEditText(mPasswordView);
        } else {
            Utils.enableEditText(mLoginView);
            Utils.enableEditText(mPasswordView);
        }
    }

    @Override
    public void onRecieveResult(int resultCode, Bundle resultData) {
        ResponseObject response = new ResponseObject(resultData.getString(RESPONSE));

        if (resultCode == STATUS_ERROR) {
            setProgress(false);
            mLoginView.setError(null);
            mPasswordView.setError((CharSequence) response.getParam(ERROR_STRING, STRING));
            mPasswordView.requestFocus();
        } else if (resultCode == STATUS_OK) {
            if (response.method().equals(LOGIN_METHOD)) {
                authDevice(response);
            } else {
                if (mLoginType == LOGIN_ADMIN) {
                    startReader();
                } else {
                    app().startLocation(_handler);
                }
            }
        } else {
            setProgress(false);
            noConnection();
        }
    }
}

