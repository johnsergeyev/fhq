package sergeyev.john.nfcscanner.classes;

public interface AppConstants {
    //ResponseObject parse types
    static final int OBJECT = 0;
    static final int ARRAY = 1;
    static final int STRING = 2;

    static final int LOCATION_ERROR = 0;
    static final int LOCATION_OK = 1;
    static final int LOCATION_NO_SIGNAL = 2;
    static final int LOCATION_NO_WIFI = 3;

    //NFC types
    static final int NFC_ERROR = -3;
    static final int NFC_OFF = -2;
    static final int NFC_ON = -1;

    static final String USER_AGENT = "Mozilla/5.0";

    static final String PREFS = "preferences";
    static final String OFFLINE_TAGS = "offline_tags";
    static final String STATUS = "status";
    static final String OK = "ok";
    static final String DONE = "done";
    static final String STAGE = "counter";
    static final String TOTAL = "total";
    static final String SUBSCRIPTION = "subscription";
    static final String TASK = "task";
    static final String COORDS = "coords";
    static final String LAT = "lat";
    static final String LONG = "long";
    static final String DISTANCE = "distance";
    static final String PLACE = "place";
    static final String NAME = "name";
    static final String SURNAME = "surname";
    static final String SECONDNAME = "second_name";
    static final String TOKEN = "token";
    static final String PLAYER = "player";
    static final String USER_ID = "userid";
    static final String REGISTERED = "registered";
    static final String ERROR_STRING = "errorstr";
    static final String ERROR_CODE = "statuscode";
    static final String RECIEVER = "reviever";
    static final String RESPONSE = "response";
    static final String METHOD = "method";
    static final String PARAMS = "params";
    static final String SERVER_TAG = "ServerProtocol";
    static final String API_URL = "http://fh.insbrook.com/api/";
    static final String LOGIN_METHOD = "user.php";
    static final String AUTH_TAG_METHOD = "markreg.php";
    static final String DEVICE_AUTH_METHOD = "device.php";
    static final String ADMIN_TAG_METHOD = "markadmin.php";
    static final String ADMIN_SUB_METHOD = "marksub.php";
    static final String COACH_TAG_METHOD = "mark.php";

    static final String LOGIN_TYPE = "login_type";
    static final int LOGIN_ADMIN = 0;
    static final int LOGIN_COACH = 1;
    static final String LOGIN_ADMIN_TEXT = "admin";
    static final String LOGIN_COACH_TEXT = "coach";

    //Connection states
    static final int STATUS_NO_CONNECTION = -1;
    static final int STATUS_OK = 1;
    static final int STATUS_ERROR = 0;

    //Tag types
    static final String TAG_TYPE = "tagtype";
    static final String TAG = "tag";
    static final int TAG_KNOWN_UNREGISTERED = 0;
    static final int TAG_KNOWN_REGISTERED = 1;
    static final int TAG_UNKNOWN = 2;

    static final int TOKEN_ERROR = 100;
}
