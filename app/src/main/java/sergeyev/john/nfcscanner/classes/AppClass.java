package sergeyev.john.nfcscanner.classes;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;

public class AppClass extends Application {
    private String _token;
    private CoachClass _coach;

    public void setToken(String token) {
        _token = token;
    }

    public String getToken() {
        return _token;
    }

    public void setCoachParams(double lt, double lg, double d, String uid) {
        if (_coach == null) {
            _coach = new CoachClass(lt, lg, d, uid, getApplicationContext());
        } else {
            _coach.update(lt, lg, d, uid);
        }
    }

    public CoachClass getCoachParams() {
        return _coach;
    }

    public void setCoachHandler(Handler _h) {
        if (_coach != null) {
            _coach.setHandler(_h);
        }
    }

    public void startLocation(Handler _h) {
        if (_coach != null) {
            _coach.start(_h);
        }
    }

    public void stopLocation() {
        if (_coach != null) {
            _coach.stop();
        }
    }

    public String getDeviceId() {
        return Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getCaption() {
        return Utils.concatStrings(Build.MANUFACTURER,"_",Build.MODEL);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
