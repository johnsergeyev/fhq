package sergeyev.john.nfcscanner.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseObject implements AppConstants {
    private JSONObject _json;

    public ResponseObject(String response) {
        try {
            _json = new JSONObject(response);
        } catch (JSONException e) {

        }
    }

    public JSONObject json() {
        return _json;
    }

    public String method() {
        try {
            return _json.getString(METHOD);
        } catch (JSONException e) {
           return "";
        }
    }

    public <T> T getParam(String name, int type) {
        switch (type) {
            case OBJECT:
                try {
                    return (T) _json.getJSONObject(name);
                } catch (JSONException e) {
                    return null;
                }
            case ARRAY:
                try {
                    return (T) _json.getJSONArray(name);
                } catch (JSONException e) {
                    return null;
                }
            case STRING:
                try {
                    return (T) _json.getString(name);
                } catch (JSONException e) {
                    return (T) "";
                }
        }

        return null;
    }
}
