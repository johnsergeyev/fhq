package sergeyev.john.nfcscanner.classes;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class ServiceResultReciever extends ResultReceiver {

	private Reciever mReciever;

	public ServiceResultReciever(Handler handler) {
		super(handler);
	}

	public void setReceiver(Reciever receiver) {
		mReciever = receiver;
	}
	
	public Reciever getReciever() {
		return mReciever;
	}
	
	public interface Reciever {
		public void onRecieveResult(int resultCode, Bundle resultData);
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		if (mReciever != null) {
			mReciever.onRecieveResult(resultCode, resultData);
		}
	}
}
