package sergeyev.john.nfcscanner.classes;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class CoachClass implements AppConstants {
    public static final String TAG = "couchClass";

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 5 meters
    private static final long MIN_TIME_BW_UPDATES = 20000; // 10 seconds

    private Location _location;
    private Location _lastKnownLocation;
    private double _distance;
    private Handler _handler;
    private Boolean _active;
    private String _uid;

    private LocationListener _locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            parseLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (checkEnabled()) {
                parseLocation(_locationManager.getLastKnownLocation(provider));
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }
    };

    private LocationManager _locationManager;

    private Boolean checkEnabled() {
        Boolean location = _locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                _locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!location) {
            return false;
        }

        return true;
    }

    private void parseLocation(Location location) {
        if (location == null) return;

        _lastKnownLocation = location;

        double d = Math.abs(_location.distanceTo(location));
        Log.d(TAG, Utils.concatStrings("location: lt:", String.valueOf(location.getLatitude()),
                " lg:"+String.valueOf(location.getLongitude()), " distance:", String.valueOf(d), "m"));

        if (d > _distance) {
            if (_handler != null) {
                Message m = new Message();
                Bundle b = new Bundle();
                m.what = LOCATION_ERROR;
                b.putDouble(DISTANCE, d - _distance);
                m.setData(b);
                _handler.sendMessage(m);
            }
            stop();
        } else {
            if (_handler != null) _handler.sendEmptyMessage(LOCATION_OK);
        }
    }

    public String getUID() {
        return _uid;
    }

    public void update(double lattitude, double longitute, double distance, String uid) {
        _location = new Location("start");
        _location.setLatitude(lattitude);
        _location.setLongitude(longitute);
        _uid = uid;
        _distance = distance;
    }

    public CoachClass(double lattitude, double longitute, double distance, String uid, Context context) {
        _active = false;
        update(lattitude, longitute, distance, uid);
        _locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public Location lastKnownLocation() {
        return _lastKnownLocation;
    }

    public void setHandler(Handler h) {
        if (_handler != null &&_handler.equals(h)) return;
        _handler = h;
        Log.d(TAG, "new handler");
    }

    public void start(Handler h) {
        if (_active) return;
        _active = true;
        setHandler(h);
        Log.d(TAG, "started");

        if (checkEnabled()) {
            _locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES,
                    _locationListener);

            _locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES,
                    _locationListener);
        } else {
            if (_handler != null) {
                _handler.sendEmptyMessage(LOCATION_NO_SIGNAL);
            }
        }
    }

    public void stop() {
        if (!_active) return;

        _active = false;
        if (_locationManager == null) return;

        Log.d(TAG, "stoped");

        _handler = null;
        _locationManager.removeUpdates(_locationListener);
    }
}
