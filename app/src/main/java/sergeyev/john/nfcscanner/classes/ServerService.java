package sergeyev.john.nfcscanner.classes;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class ServerService extends IntentService implements AppConstants {

    public ServerService() {
        super("server_service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ResultReceiver reciever = intent.getParcelableExtra(RECIEVER);
        String method = intent.getStringExtra(METHOD);
        String params = intent.getStringExtra(PARAMS);

        HttpSender http = new HttpSender(10000);
        String m = Utils.concatStrings(API_URL, method);
        String p;

        if (params != null) {
            p = Utils.concatStrings(params, "&token=", app().getToken());
        } else {
            p = Utils.concatStrings("token=", app().getToken());
        }

        p = Utils.concatStrings(p, "&deviceId=", app().getDeviceId());

        Log.d(SERVER_TAG, ">> : " + m + "?" + p);

        JSONObject result = (JSONObject) http.getRequest(m, p);

        int code;
        String response = "";
        Bundle b = new Bundle();

        if (result == null) {
            code = STATUS_NO_CONNECTION;
        } else {
            try {
                if (result.has("status") &&
                        result.getString("status").equals("error")) {
                    code = STATUS_ERROR;
                } else {
                    code = STATUS_OK;
                }
            } catch (JSONException e) {
                code = STATUS_ERROR;
            }
            try {
                result.put(METHOD, method);
            } catch (JSONException e) {

            }
            response = result.toString();
        }

        Log.d(SERVER_TAG, "<< : " + response);

        b.putString(RESPONSE, response);
        if (reciever != null)
            reciever.send(code, b);
    }

    private AppClass app() {
        return (AppClass) getApplication();
    }
}
