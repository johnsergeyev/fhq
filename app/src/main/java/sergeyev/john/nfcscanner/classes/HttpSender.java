package sergeyev.john.nfcscanner.classes;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class HttpSender implements AppConstants {
	static final int MIN_DELAY = 0;

	private int _timeout;
	private int _start;
	private int _finish;

	public HttpSender(int timeout) {
		_timeout = timeout;
	}

	public JSONObject getRequest(String url, String get_request) {
		try {
			Calendar c = Calendar.getInstance();
			_start = c.get(Calendar.MILLISECOND);
			String getURL = (get_request.equalsIgnoreCase("")) ? (url) : (url + "?" + getEncodedGetRequest(get_request));
			URL urlObject = new URL(getURL);
			HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
			connection.setConnectTimeout(_timeout);
			connection.setReadTimeout(_timeout);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = 0;

			responseCode = connection.getResponseCode();

			_finish = c.get(Calendar.MILLISECOND);

			if (_finish - _start < MIN_DELAY) {
				try {
					Thread.sleep(MIN_DELAY - (_finish - _start));
				} catch (InterruptedException e) {

				}
			}

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				JSONObject jsonResult = null;
				jsonResult = new JSONObject(response.toString());
				jsonResult = jsonResult.getJSONObject("response");
				return jsonResult;
			} else {
				return null;
			}
		} catch (IOException e) {
			return null;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getEncodedGetRequest(String get_request) {
		String[] pieces = get_request.split("\\&");
		String str = "";

		for (int i = 0; i < pieces.length; i++) {
			if (str!="") str+="&";
			int _eq = pieces[i].indexOf("=");
			String param = pieces[i].substring(0, _eq);
			String value = pieces[i].substring(_eq+1);
			str+=param+"="+Uri.encode(value);
		}
		return str;
	}
}
