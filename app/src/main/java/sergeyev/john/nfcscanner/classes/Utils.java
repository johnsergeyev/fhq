package sergeyev.john.nfcscanner.classes;

import android.text.InputType;
import android.widget.EditText;

public class Utils {
    public static String concatStrings(String... strings) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            buffer.append(strings[i]);
        }
        return buffer.toString();
    }

    public static void disableEditText(EditText et) {
        disableEditText(et, false);
    }

    public static void disableEditText(EditText et, Boolean noLock) {
        et.setFocusable(false);
        et.setFocusableInTouchMode(false);
        et.setEnabled(noLock);
    }

    public static void enableEditText(EditText et) {
        et.setFocusable(true);
        et.setFocusableInTouchMode(true);
        et.setEnabled(true);
    }

    public static <T> boolean contains(final T[] array, final T v) {
        if (v == null) {
            for (final T e : array)
                if (e == null)
                    return true;
        } else {
            for (final T e : array)
                if (e == v || v.equals(e))
                    return true;
        }

        return false;
    }
}
