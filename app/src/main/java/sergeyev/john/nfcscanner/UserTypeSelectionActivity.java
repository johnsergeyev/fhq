package sergeyev.john.nfcscanner;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import sergeyev.john.nfcscanner.classes.AppConstants;

public class UserTypeSelectionActivity extends AppCompatActivity implements AppConstants {

    private int checkForSettings(Boolean skipGps) {
        NfcManager manager = (NfcManager) getApplicationContext().getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null) {
            if (adapter.isEnabled()) {
                if (skipGps) return NFC_ON;
                return checkForLocation();
            } else {
                return NFC_OFF;
            }
        } else {
            return NFC_ERROR;
        }
    }

    private int checkForLocation() {
        final LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (manager == null) return LOCATION_ERROR;

        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if (!wifi.isWifiEnabled()){
            return LOCATION_NO_WIFI;
        }

        if (!manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) || !manager.isProviderEnabled( LocationManager.GPS_PROVIDER )) {
            return LOCATION_NO_SIGNAL;
        } else {
            return LOCATION_OK;
        }
    }

    private void initView() {
        getSupportActionBar().setTitle(R.string.selector_header);
        Button adminButton = (Button) findViewById(R.id.adminButton);
        Button coachButton = (Button) findViewById(R.id.coachButton);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int check = checkForSettings(v.getId() == R.id.adminButton);

                switch (check) {
                    case NFC_ERROR:
                        closeApp(NFC_ERROR);
                        return;
                    case LOCATION_ERROR:
                        closeApp(LOCATION_ERROR);
                        return;
                    case NFC_OFF:
                        openNfcSettings();
                        return;
                    case LOCATION_NO_SIGNAL:
                        openGPSSettings();
                        return;
                    case LOCATION_NO_WIFI:
                        openWiFiSettings();
                        return;
                }

                switch (v.getId()) {
                    case R.id.adminButton:
                        openAdminActivity();
                        break;
                    case R.id.coachButton:
                        openCoachActivity();
                        break;
                }
            }
        };

        adminButton.setOnClickListener(onClickListener);
        coachButton.setOnClickListener(onClickListener);
    }

    private void openNfcSettings() {
        Toast.makeText(this, R.string.nfc_notify, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
    }

    private void openWiFiSettings() {
        Toast.makeText(this, R.string.wifi_notify, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    private void openGPSSettings() {
        Toast.makeText(this, R.string.location_notify, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private void closeApp(int error) {
        Toast.makeText(this, (error == NFC_ERROR)?
                R.string.nfc_error:R.string.location_error, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void openAdminActivity() {
        openLoginActivity(LOGIN_ADMIN);
    }

    private void openCoachActivity() {
        openLoginActivity(LOGIN_COACH);
    }

    private void openLoginActivity(int type) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra(LOGIN_TYPE, type);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type_selection);

        initView();
    }
}
