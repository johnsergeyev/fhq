package sergeyev.john.nfcscanner;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;

import sergeyev.john.nfcscanner.classes.AppClass;
import sergeyev.john.nfcscanner.classes.AppConstants;
import sergeyev.john.nfcscanner.classes.CoachClass;
import sergeyev.john.nfcscanner.classes.ResponseObject;
import sergeyev.john.nfcscanner.classes.ServerService;
import sergeyev.john.nfcscanner.classes.ServiceResultReciever;
import sergeyev.john.nfcscanner.classes.Utils;


public class NFCReadActivity extends AppCompatActivity implements AppConstants, ServiceResultReciever.Reciever {
    private ServiceResultReciever mReciever;
    private Boolean _tagDetected;
    private String _tag;
    private TextView _tv;
    private int _loginType;
    private Handler _handler;
    private int _repeat;
    private Boolean _offlineMode;
    private String[] _offlineArray;
    private int _currentOfflineIndex;
    private Boolean _hasReciever;

    // list of NFC technologies detected:
    private final String[][] techList = new String[][] {
            new String[] {
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(), Ndef.class.getName()
            }
    };

    private BroadcastReceiver _reciever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcread);
        _offlineMode = false;
        _offlineArray = null;
        _hasReciever = false;
        _repeat = 0;
        _tagDetected = false;
        _tv = (TextView) findViewById(R.id.tip);
        mReciever = new ServiceResultReciever(new Handler());
        mReciever.setReceiver(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            _loginType = extras.getInt(LOGIN_TYPE);
            if (_loginType == LOGIN_COACH) {
                _handler = new Handler(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        switch (msg.what) {
                            case LOCATION_ERROR:
                                locationError();
                                finish();
                                return true;
                            case LOCATION_NO_SIGNAL:
                                signalError();
                                finish();
                                return true;
                            case LOCATION_OK:
                                if (_offlineMode) {
                                    processOffline();
                                }
                                return true;
                        }
                        return false;
                    }
                });
                _currentOfflineIndex = 0;
                _offlineMode = true;
                processOffline();
                app().setCoachHandler(_handler);
            }
        }

        _reciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (app().isOnline()) {
                    pauseInternetIntent();
                    app().startLocation(_handler);
                }
            }
        };

        setupActionBar();
    }

    private void signalError() {
        Toast.makeText(getBaseContext(), R.string.error_location_settings, Toast.LENGTH_SHORT).show();
    }

    private void otherError() {
        Toast.makeText(getBaseContext(), getString(R.string.data_error), Toast.LENGTH_SHORT).show();
        tagRequestError();
    }

    private void locationError() {
        Toast.makeText(getBaseContext(), R.string.error_location, Toast.LENGTH_SHORT).show();
    }

    private AppClass app() {
        return (AppClass) getApplication();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setNFCIntent();
        if (_loginType == LOGIN_COACH) {
            if (!_offlineMode) {
                app().startLocation(_handler);
            }
        }
    }

    private void noConnection() {
        Toast.makeText(getBaseContext(), R.string.error_no_connection, Toast.LENGTH_SHORT).show();
        _tv.setText(R.string.instructions);
    }

    private void pauseNFCIntent() {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcAdapter.disableForegroundDispatch(this);
    }

    private void setNFCIntent() {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
    }

    private void setInternerIntent() {
        if (_hasReciever) return;
        Log.d(TAG, "start internet intent");
        _hasReciever = true;
        registerReceiver(_reciever, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    private void pauseInternetIntent() {
        if (!_hasReciever) return;
        Log.d(TAG, "stop internet intent");
        _hasReciever = false;
        unregisterReceiver(_reciever);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseNFCIntent();
        if (_loginType == LOGIN_COACH) {
            app().stopLocation();
        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
            if (_tagDetected == true) return;
            if (_loginType == LOGIN_ADMIN) {
                detectTag(intent);
            } else {
                new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Dialog_Alert)
                        .setTitle(getString(R.string.message))
                        .setMessage(getString(R.string.dialog_question))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                detectTag(intent);
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                _tagDetected = false;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

    private void detectTag(Intent intent) {
        _tag = ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));

        if (!app().isOnline()) {
            if (_loginType == LOGIN_ADMIN) {
                noConnection();
            } else {
                offlineDetect(_tag);
            }
            return;
        }

        _tagDetected = true;
        _tv.setText(R.string.server_connect);
        startNewServerIntent(_tag);
    }

    private void startNewServerIntent(String tag) {
        Intent intent = new Intent(NFCReadActivity.this, ServerService.class);
        intent.putExtra(RECIEVER, mReciever);
        intent.putExtra(METHOD, _loginType == LOGIN_ADMIN ? ADMIN_TAG_METHOD : COACH_TAG_METHOD);
        intent.putExtra(PARAMS, formTagString(tag));
        startService(intent);
    }

    private void offlineDetect(String tag) {
        offlineDetect(tag, false);
    }

    private void offlineDetect(String tag, Boolean autoRequest) {
        pauseInternetIntent();
        app().stopLocation();

        SharedPreferences shared = getApplicationContext().getSharedPreferences(PREFS, 0);
        String offline = shared.getString(OFFLINE_TAGS, "");
        if (offline.equals("")) {
            offline = tag;
        } else {
            String[] arr = offline.split(",");
            if (Utils.contains(arr, tag)) {
                if (!autoRequest) {
                    Toast.makeText(getBaseContext(), R.string.tag_aready_offline, Toast.LENGTH_SHORT).show();
                }
                continueDetection();
                return;
            }
            offline = Utils.concatStrings(offline, ",", tag);
        }
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(OFFLINE_TAGS, offline);
        editor.commit();

        Toast.makeText(getBaseContext(), R.string.tag_offline_save, Toast.LENGTH_SHORT).show();
        continueDetection();
    }

    private void continueDetection() {
        if (_offlineArray == null) {
            _offlineMode = true;
            setInternerIntent();
        } else {
            parseNext();
        }
    }

    private void processOffline() {
        if (_offlineArray != null) return;
        if (!app().isOnline()) return;

        _tagDetected = true;

        SharedPreferences shared = getApplicationContext().getSharedPreferences(PREFS, 0);
        String offline = shared.getString(OFFLINE_TAGS, "");

        if (!offline.equals("")) {
            _offlineArray = offline.split(",");
            SharedPreferences.Editor editor = shared.edit();
            editor.remove(OFFLINE_TAGS);
            editor.commit();

            getSupportActionBar().setTitle(R.string.title_activity_nfcread_offline);
            _currentOfflineIndex = 0;
            parseNext();
        } else {
            stopOffline();
        }
    }

    private void parseNext() {
        if (_offlineArray == null ||_currentOfflineIndex == _offlineArray.length) {
            Log.d(TAG, "limit reached");
            _offlineArray = null;
            processOffline();
            return;
        }

        _tv.setText(Utils.concatStrings(String.valueOf(_currentOfflineIndex + 1), " ", getString(R.string.from), " ", String.valueOf(_offlineArray.length)));

        _tag = _offlineArray[_currentOfflineIndex];
        Log.d(TAG, "offline parse tag");
        startNewServerIntent(_tag);
    }

    private void stopOffline() {
        Log.d(TAG, "stop offline");
        _offlineArray = null;
        _tagDetected = false;
        _offlineMode = false;
        getSupportActionBar().setTitle(R.string.title_activity_nfcread);
        tagRequestError();
    }

    private String formTagString(String tag) {
        if (_loginType == LOGIN_ADMIN) {
            return Utils.concatStrings("markId=", tag);
        } else {
            CoachClass coach = app().getCoachParams();
            return Utils.concatStrings("markId=", tag, "&lat=", String.valueOf(coach.lastKnownLocation().getLatitude()),
                    "&long=", String.valueOf(coach.lastKnownLocation().getLongitude()), "&userid="+coach.getUID());
        }
    }

    private String ByteArrayToHexString(byte [] inarray) {
        int i, j, in;
        String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String out= "";

        for(j = 0 ; j < inarray.length ; ++j)
        {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    private void tagRequestError() {
        _tv.setText(R.string.instructions);
    }

    @Override
    public void onRecieveResult(int resultCode, Bundle resultData) {
        ResponseObject response = new ResponseObject(resultData.getString(RESPONSE));

        _tagDetected = false;

        if (resultCode == STATUS_ERROR || resultCode == STATUS_OK) {
            if (_offlineMode) {
                _currentOfflineIndex++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        parseNext();
                    }
                }, 500);
                return;
            }
            Intent intent = new Intent(getBaseContext(), DataActivity.class);
            if (resultCode == STATUS_ERROR) {
                try {
                    if (response.json().getInt(ERROR_CODE) == 111) {
                        if (_loginType == LOGIN_ADMIN) {
                            intent.putExtra(TAG_TYPE, TAG_UNKNOWN);
                        } else {
                            _repeat += 1;
                            if (_repeat > 1) {
                                _repeat = 0;
                                Toast.makeText(getBaseContext(), (String) response.getParam(ERROR_STRING, STRING), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getBaseContext(), R.string.nfc_error_text, Toast.LENGTH_SHORT).show();
                            }
                            tagRequestError();
                            return;
                        }
                    } else if (response.json().getInt(ERROR_CODE) == TOKEN_ERROR) {
                        _repeat = 0;
                        tokenError();
                        return;
                    } else {
                        _repeat = 0;
                        Toast.makeText(getBaseContext(), (String) response.getParam(ERROR_STRING, STRING), Toast.LENGTH_SHORT).show();
                        tagRequestError();
                        return;
                    }
                } catch (JSONException e) {
                    _repeat = 0;
                    Toast.makeText(getBaseContext(), R.string.nfc_error_text, Toast.LENGTH_SHORT).show();
                    tagRequestError();
                    return;
                }
            } else {
                _repeat = 0;
                if (_loginType == LOGIN_ADMIN) {
                    try {
                        JSONObject player = response.getParam(PLAYER, OBJECT);
                        if (player.getInt(REGISTERED) == 0) {
                            intent.putExtra(TAG_TYPE, TAG_KNOWN_UNREGISTERED);
                        } else {
                            intent.putExtra(TAG_TYPE, TAG_KNOWN_REGISTERED);
                        }

                        intent.putExtra(SURNAME, (String) player.getString("f"));
                        intent.putExtra(NAME, (String) player.getString("i"));
                        intent.putExtra(SECONDNAME, (String) player.getString("o"));
                        intent.putExtra(STAGE, Utils.concatStrings(String.valueOf(player.getInt(STAGE)), "/", String.valueOf(player.getInt(TOTAL))));
                        intent.putExtra(SUBSCRIPTION, player.getInt(SUBSCRIPTION));
                        intent.putExtra(PLACE, player.getInt(PLACE));
                    } catch (JSONException e) {
                        otherError();
                        return;
                    }
                } else {
                    if (response.getParam(STATUS, STRING).equals(OK)) {
                        // пройдено задание
                        _repeat = 0;
                        Toast.makeText(getBaseContext(), R.string.user_quest_complete, Toast.LENGTH_SHORT).show();
                        tagRequestError();
                        return;
                    } else if (response.getParam(STATUS, STRING).equals(DONE)) {
                        // финиш
                        _repeat = 0;
                        try {
                            JSONObject player = response.getParam(PLAYER, OBJECT);
                            intent.putExtra(TAG_TYPE, TAG_KNOWN_REGISTERED);
                            intent.putExtra(SURNAME, (String) player.getString("f"));
                            intent.putExtra(NAME, (String) player.getString("i"));
                            intent.putExtra(SECONDNAME, (String) player.getString("o"));
                            intent.putExtra(STAGE, String.valueOf(player.getInt(STAGE)));
                            intent.putExtra(PLACE, player.getInt(PLACE));
                        } catch (JSONException e) {
                            otherError();
                            return;
                        }
                    }
                }
            }
            intent.putExtra(TAG, _tag);
            startActivityForResult(intent, 1);
            _tv.setText(R.string.instructions);
        } else {
            if (_loginType == LOGIN_ADMIN) {
                noConnection();
            } else {
                offlineDetect(_tag, true);
            }
        }
    }

    private void tokenError() {
        Toast.makeText(getBaseContext(), R.string.token_error, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == TOKEN_ERROR) {
            tokenError();
        }
        if (_loginType == LOGIN_COACH) {
            app().setCoachHandler(_handler);
        }
    }
}
